<?php

namespace Database\Seeders;

use App\Models\Province;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::truncate();//kosongkan table

        $key = env('RAJAONGKIR_API_KEY');//Buat akun atau pakai API akun

        $province_url = 'https://api.rajaongkir.com/starter/province';

        //logic untuk get province and city
        $getProvinces = $this->getData($key,$province_url);
        $provinces = $getProvinces['rajaongkir']['results'];

        foreach($provinces as $province){
            $data[] = [
                'id' => $province['province_id'],
                'province' => $province['province']
                // 'created_at' => date('Y-m-d H:i:s')
            ];
        }

        Province::insert($data);
    }

     //function untuk get data province and city
    private function getData($key,$url){
        return Http::withHeaders([
            'key' => $key
        ])->get($url);
    }
}
