<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::truncate();//kosongkan table

        $key = env('RAJAONGKIR_API_KEY'); //Buat akun atau pakai API akun
        $city_url = 'https://api.rajaongkir.com/starter/city';

        //logic untuk get province and city
        $getCities = $this->getData($key,$city_url);
        $cities = $getCities['rajaongkir']['results'];

        foreach($cities as $city){
            $data[] = [
                'id' => $city['city_id'],
                'city_name' => $city['city_name'],
                'province_id' => $city['province_id'],
                'type' => $city['type'],
                'postal_code' => $city['postal_code']
                // 'created_at' => date('Y-m-d H:i:s')
            ];
        }

        City::insert($data);
    }

     //function untuk get data province and city
    private function getData($key,$url){
        return Http::withHeaders([
            'key' => $key
        ])->get($url);
    }

}
