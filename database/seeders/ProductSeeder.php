<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();

        /* Skysea Scary Crime */
        Product::create([
            'color_id' => 1,
            'category_id' => 1,
            'size_id' => 1,
            'name' => 'Skysea Scary Crime',
            'price' => 105000,
            'stock' => 12,
            'image' => 'product-images\T-Shirt Skysea Scary Crime.jpg',
            'description' => 'Skysea Alien Invasion dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 2,
            'category_id' => 1,
            'size_id' => 1,
            'name' => 'Skysea Scary Crime',
            'price' => 105000,
            'stock' => 12,
            'image' => 'product-images\T-Shirt Skysea Scary Crime.jpg',
            'description' => 'Skysea Alien Invasion dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 3,
            'category_id' => 1,
            'size_id' => 1,
            'name' => 'Skysea Scary Crime',
            'price' => 105000,
            'stock' => 12,
            'image' => 'product-images\T-Shirt Skysea Scary Crime.jpg',
            'description' => 'Skysea Alien Invasion dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 1,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Scary Crime',
            'price' => 105000,
            'stock' => 12,
            'image' => 'product-images\T-Shirt Skysea Scary Crime.jpg',
            'description' => 'Skysea Alien Invasion dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 2,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Scary Crime',
            'price' => 105000,
            'stock' => 12,
            'image' => 'product-images\T-Shirt Skysea Scary Crime.jpg',
            'description' => 'Skysea Alien Invasion dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 3,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Scary Crime',
            'price' => 105000,
            'stock' => 12,
            'image' => 'product-images\T-Shirt Skysea Scary Crime.jpg',
            'description' => 'Skysea Alien Invasion dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        /* Skysea OKAMI Jappanese Edition */
        Product::create([
            'color_id' => 1,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea OKAMI Jappanese Edition',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\OKAMI JAPPANESE EDITION.jpg',
            'description' => 'Skysea Scary Crime dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 2,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea OKAMI Jappanese Edition',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\OKAMI JAPPANESE EDITION.jpg',
            'description' => 'Skysea Scary Crime dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 3,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea OKAMI Jappanese Edition',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\OKAMI JAPPANESE EDITION.jpg',
            'description' => 'Skysea Scary Crime dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 1,
            'category_id' => 1,
            'size_id' => 3,
            'name' => 'Skysea OKAMI Jappanese Edition',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\OKAMI JAPPANESE EDITION.jpg',
            'description' => 'Skysea Scary Crime dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 3,
            'category_id' => 1,
            'size_id' => 3,
            'name' => 'Skysea OKAMI Jappanese Edition',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\OKAMI JAPPANESE EDITION.jpg',
            'description' => 'Skysea Scary Crime dengan kualitas terbaik, nyaman untuk digunakan'
        ]);

        /* BOXER PANTS */
        Product::create([
            'color_id' => 4,
            'category_id' => 2,
            'size_id' => 2,
            'name' => 'BOXER PANTS',
            'price' => 20000,
            'stock' => 20,
            'image' => 'product-images\boxer pants.jpg',
            'description' => 'Skysea Scary Crime dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        Product::create([
            'color_id' => 4,
            'category_id' => 2,
            'size_id' => 3,
            'name' => 'BOXER PANTS',
            'price' => 20000,
            'stock' => 20,
            'image' => 'product-images\boxer pants.jpg',
            'description' => 'Skysea Scary Crime dengan kualitas terbaik, nyaman untuk digunakan'
        ]);
        /* Skysea Adventure */
        Product::create([
            'color_id' => 2,
            'category_id' => 1,
            'size_id' => 3,
            'name' => 'Skysea Adventure',
            'price' => 90000,
            'stock' => 10,
            'image' => 'product-images\adventure.jpg',
            'description' => 'The adventure begins, where are we going this weekend? definitely on vacation by always wearing a Skysea shirt on every trip'
        ]);
        Product::create([
            'color_id' => 3,
            'category_id' => 1,
            'size_id' => 3,
            'name' => 'Skysea Adventure',
            'price' => 90000,
            'stock' => 10,
            'image' => 'product-images\adventure.jpg',
            'description' => 'The adventure begins, where are we going this weekend? definitely on vacation by always wearing a Skysea shirt on every trip'
        ]);
        Product::create([
            'color_id' => 2,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Adventure',
            'price' => 90000,
            'stock' => 10,
            'image' => 'product-images\adventure.jpg',
            'description' => 'The adventure begins, where are we going this weekend? definitely on vacation by always wearing a Skysea shirt on every trip'
        ]);
        Product::create([
            'color_id' => 3,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Adventure',
            'price' => 90000,
            'stock' => 10,
            'image' => 'product-images\adventure.jpg',
            'description' => 'The adventure begins, where are we going this weekend? definitely on vacation by always wearing a Skysea shirt on every trip'
        ]);

        /* Skysea Scooter */
        Product::create([
            'color_id' => 4,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Scooter',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\Scooter.jpg',
            'description' => 'Yuk jalan-jakan mengelilingi kota dengan Series Scooter dari SKYSEA.'
        ]);
        Product::create([
            'color_id' => 5,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Scooter',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\Scooter.jpg',
            'description' => 'Yuk jalan-jakan mengelilingi kota dengan Series Scooter dari SKYSEA.'
        ]);
        Product::create([
            'color_id' => 4,
            'category_id' => 1,
            'size_id' => 3,
            'name' => 'Skysea Scooter',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\Scooter.jpg',
            'description' => 'Yuk jalan-jakan mengelilingi kota dengan Series Scooter dari SKYSEA.'
        ]);
        Product::create([
            'color_id' => 5,
            'category_id' => 1,
            'size_id' => 3,
            'name' => 'Skysea Scooter',
            'price' => 100000,
            'stock' => 10,
            'image' => 'product-images\Scooter.jpg',
            'description' => 'Yuk jalan-jakan mengelilingi kota dengan Series Scooter dari SKYSEA.'
        ]);
        /* Skysea Freaking of Die */
        Product::create([
            'color_id' => 5,
            'category_id' => 1,
            'size_id' => 2,
            'name' => 'Skysea Freaking of Die',
            'price' => 95000,
            'stock' => 15,
            'image' => 'product-images\Freaking of Die.jpg',
            'description' => 'Behind fear there is beauty in it. Agles with the clothes Skysea Freaking of Die.'
        ]);
        Product::create([
            'color_id' => 5,
            'category_id' => 2,
            'size_id' => 3,
            'name' => 'Skysea Scooter',
            'price' => 95000,
            'stock' => 10,
            'image' => 'product-images\Freaking of Die.jpg',
            'description' => 'Behind fear there is beauty in it. Agles with the clothes Skysea Freaking of Die.'
        ]);
    }
}
