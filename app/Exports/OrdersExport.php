<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrdersExport implements FromView
{
    protected $orders;
    public function __construct($orders)
    {
        $this->orders = $orders;
    }
    public function view(): View
    {
        return view('admin.dashboard.export-excel', [
            'orders' => $this->orders
        ]);
    }
}
