<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Payment;
use App\Models\Order;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListOrderController extends Controller
{
    public function index()
    {
        // $code = Cart::where('user_id',Auth::user()->id)
        // ->get();
        // $unique_code = $code->unique_code;
        return view('customer.order',[
            'active' => 'listorder',
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get(),
            'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Cart')->get(),
            'categories' => Category::all(),

            'carts' => Cart::where('user_id',Auth::user()->id)->where('status','=','Ordered')->groupBy('unique_code')->orderBy('date','desc')->get(),
            'products' => Cart::where('user_id',Auth::user()->id)->where('status','=','Ordered')->get(),
            'count_order' => Order::whereIn('unique_code',function($query){
                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','Ordered');
            })->count(),
        ]);
    }

    public function order_details($unique_code)
    {
        return view('customer.order-details',[
            'active' => 'listorder',
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get(),
            'count_order' => Order::whereIn('unique_code',function($query){
                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','Ordered');
            })->count(),

            'orders' => Cart::where('unique_code',$unique_code)->get(),
            'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Cart')->get(),
            'categories' => Category::all(),
            'order' => Order::where('unique_code',$unique_code)->first(),
            'order_cart' => Cart::where('unique_code',$unique_code)->where('status','Ordered')->get()

        ]);
    }

    public function order_invoice($unique_code)
    {
        return view('customer.order-invoice',[
            'order' => Order::where('unique_code',$unique_code)->first(),
            'order_cart' => Cart::where('unique_code',$unique_code)->where('status','Ordered')->get()
        ]);
    }

}
