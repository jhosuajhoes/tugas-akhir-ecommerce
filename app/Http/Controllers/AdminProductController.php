<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\Color;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use GuzzleHttp\Handler\Proxy;
use RealRashid\SweetAlert\Facades\Alert;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index',[
            'active'=>'product',
            'products'=>Product::groupBy('name')->get(),
        ]);
    }

    public function index_product($name)
    {
        return view('admin.products.product.index',[
            'active'=>'product',
            'product' => Product::where('name',$name)->first(),
            'products'=>Product::where('name',$name)->get(),
            'sizes'=>Size::all(),
            'colors'=>Color::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create',[
            'active'=>'product',
            'categories'=>Category::all(),
            'sizes'=>Size::all(),
            'colors'=>Color::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ddd($request);
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'category_id' => 'required|max:255',
            'color_id' => 'required|max:255',
            'size_id' => 'required|max:255',
            'price' => 'required|max:255',
            'stock' => 'required|max:255',
            'description' => 'required|max:255',
            'image' => 'required|image|file|max:5120'
        ]);
        if($request->file('image')){
            $validatedData['image'] = $request->file('image')->store('product-images');
        }

        Product::create($validatedData);
        return redirect('/admin/product')->with('toast_success', 'Product Inserted!');
    }

    public function store_product(Request $request)
    {
        // dd($request);
        /* cek apakah produk varian sudah ada atau belum */
        $name = $request->name;
        $color = $request->color_id;
        $size = $request->size_id;
        $cek_produk = Product::where('name',$name)->where('color_id',$color)->where('size_id',$size)->count();

        if ($cek_produk > 0) {
            Alert::warning('Product Exist', 'Maaf produk dengan warna atau ukuran terpilih sudah tersedia, silahkan update stok produk');
            return redirect()->back();
        }

        $validatedData = $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'color_id' => 'required',
            'size_id' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        Product::create($validatedData);
        return redirect()->back()->with('toast_success', 'New variant inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.products.show',[
            'active' => 'product',
            'product' => Product::where('id',$id)->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.products.product.edit',[
            'active' => 'product',
            'product' => Product::where('id',$id)->first(),
            'categories'=>Category::all(),
            'sizes'=>Size::all(),
            'colors'=>Color::all()
        ]);
    }

    public function edit_all($name)
    {
        return view('admin.products.edit',[
            'active' => 'product',
            'product' => Product::where('name',$name)->first(),
            'categories'=>Category::all(),
            'sizes'=>Size::all(),
            'colors'=>Color::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'color_id' => 'required|max:255',
            'size_id' => 'required|max:255',
            'stock' => 'required|max:255',
        ]);
        $name = $request->name;
        Product::where('id',$id)->update($validatedData);
        return redirect("/admin/product-show/$name")->with('toast_success', 'Product Updated!');
    }

    public function update_all(Request $request, $name)
    {
        // dd($request);
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'category_id' => 'required|max:255',
            'price' => 'required|max:255',
            'description' => 'required|max:255',
        ]);
        $name = $request->name;
        Product::where('name',$name)->update($validatedData);
        return redirect('/admin/product')->with('toast_success', 'Product Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $name = $request->name;
        Product::destroy($id);
        return redirect("/admin/product-show/$name")->with('toast_success', 'Product deleted!');
    }

    public function destroy_all($name)
    {
        Product::where('name',$name)->delete();
        return redirect('/admin/product')->with('toast_success', 'Product deleted!');
    }

    public function image_change($id)
    {
        return view('admin.products.image-change',[
            'active' => 'product',
            'product' => Product::where('id',$id)->first()
        ]);
    }
    public function image_store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'image' => 'required|image|file|max:5120'
        ]);
        if($request->file('image')){
            $validatedData['image'] = $request->file('image')->store('product-images');
        }
        Product::where('id',$id)->update($validatedData);
        return redirect('/admin/product')->with('toast_success', 'Product Image Changed!');
    }
}
