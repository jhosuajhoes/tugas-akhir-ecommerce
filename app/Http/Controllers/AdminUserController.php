<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminUserController extends Controller
{
    public function customer()
    {
       return view('admin.manage-user.costumer',[
           'active'=>'customer',
           'users'=>User::where('roles','customer')->get()
       ]);
    }

    public function admin()
    {
       return view('admin.manage-user.admin',[
           'active'=>'admin',
           'users'=>User::where('roles','admin')->get()
       ]);
    }

    public function create_admin()
    {
       return view('admin.manage-user.create-admin',[
           'active'=>'admin'
       ]);
    }

    public function store_admin(Request $request)
    {
        // ddd($request);
        $validatedData = $request->validate([
            'name' => 'required|min:4|max:255',
            'phone_number' => 'required|min:11|max:14',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:8|max:255',
        ]);

        $validatedData['password'] = bcrypt($validatedData['password']);
        $validatedData['roles'] = 'admin';

        User::create($validatedData);

        return redirect('/admin/user/admin')->with('toast_success','New admin added');
    }
}
