<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $this->authorize('admin');
        return view('admin.dashboard.index',[
            'active' => 'dashboard',
            'customers_amount' => User::where('roles','customer')->count(),
            'products_amount' => Product::sum('stock'),
            'sold_quantity' => Cart::where('status','Ordered')->sum('quantity'),
            'incomes' => Order::where('status','!=','cancel')->where('status','!=','pending')->sum('gross_amount'),
            'total_order' => Order::count()
        ]);
    }
    public function print_transactions()
    {
        $start = request('date_start');
        $end = request('date_end');
        $status = request('status');
        if ($status == 'All') {
            $report = Order::whereBetween('date',[$start,$end])->get();
            return view('admin.dashboard.report',[
                'start' => $start,
                'end' => $end,
                'status' => $status,
                'orders' => $report
            ]);
        } else {
            $report = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
            return view('admin.dashboard.report',[
                'start' => $start,
                'end' => $end,
                'status' => $status,
                'orders' => $report
            ]);
        }
    }

    public function export_excel(Request $request)
    {
        $start = $request->date_start;
        $end = $request->date_end;
        $status = $request->status;
        if ($status == 'All') {
            $orders = Order::whereBetween('date',[$start,$end])->get();
        } else {
            $orders = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
        }
        $export = new OrdersExport($orders);
        return Excel::download($export, "order_reports($start _ $end).xlsx");
    }

    public function export_csv(Request $request)
    {
        $start = $request->date_start;
        $end = $request->date_end;
        $status = $request->status;
        if ($status == 'All') {
            $orders = Order::whereBetween('date',[$start,$end])->get();
        } else {
            $orders = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
        }
        $export = new OrdersExport($orders);
        return Excel::download($export, "order_reports($start _ $end).csv");
    }
    public function export_pdf(Request $request)
    {
        $start = $request->date_start;
        $end = $request->date_end;
        $status = $request->status;
        if ($status == 'All') {
            $orders = Order::whereBetween('date',[$start,$end])->get();
        } else {
            $orders = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
        }
        $export = new OrdersExport($orders);
        return Excel::download($export, "order_reports($start _ $end).pdf");
    }
}
