<?php

namespace App\Http\Controllers;

use Midtrans\Snap;
use App\Models\Cart;
use App\Models\City;
use App\Models\User;
use Midtrans\Config;
use App\Models\Order;
use App\Models\Category;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // dd($request->all());

        if ($request->destination && $request->weight && $request->courier) {
            $origin = 419;
            $destination = $request->destination;
            $weight = $request->weight;
            $courier = $request->courier;

            $response = Http::asForm()->withHeaders([
                'key' => env('RAJAONGKIR_API_KEY')
            ])->post('https://api.rajaongkir.com/starter/cost',[
                'origin' => $origin,
                'destination' => $destination,
                'weight' => $weight,
                'courier' => $courier
            ]);

            $cekongkir = $response['rajaongkir']['results'][0]['costs'];

        }else{
            $origin = '';
            $destination = '';
            $weight = '';
            $courier = '';
            $cekongkir=null;
        }

        $berat = Cart::where('user_id',Auth::user()->id)->where('status','Cart')->sum('quantity'); //hitung berat
        $provinsi = Province::all();
        return view('customer.shipment',[
            'active' => 'shop',
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get(),
            'count_order' => Order::whereIn('unique_code',function($query){
                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','Ordered');
            })->count(),
            'categories' => Category::all(),
            'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
            'users' => User::all(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Cart')->get(),
            'provinces'=> $provinsi,
            'cekongkir' => $cekongkir,
            'weight' => $berat
        ]);
    }

    public function ajax(Request $request)
    {
        $id = $request->province_id;
        $cities = City::where('province_id', $id)->get();
        foreach ($cities as $kota) {
            echo "<option value='$kota->id'> $kota->city_name</option>";
        }
    }

    public function courier(Request $request)
    {

        if ($request->destination && $request->weight && $request->courier) {
            $origin = 501;
            $destination = $request->destination;
            $weight = $request->weight;
            $courier = $request->courier;

            $response = Http::asForm()->withHeaders([
                'key' => env('RAJAONGKIR_API_KEY')
            ])->post('https://api.rajaongkir.com/starter/cost',[
                'origin' => $origin,
                'destination' => $destination,
                'weight' => $weight,
                'courier' => $courier
            ]);

            $cekongkir = $response['rajaongkir']['results'][0]['costs'];

        }else{
            $origin = '';
            $destination = '';
            $weight = '';
            $courier = '';
            $cekongkir=null;
        }
        echo "<option value=''>- Select Shipping Package -</option>";
        foreach ($cekongkir as $ongkir) {
            $desc = $ongkir['description'];
            $etd = $ongkir['cost'][0]['etd'];
            $cost = $ongkir['cost'][0]['value'];
            echo "<option value='$cost|$etd'>$desc - Rp$cost - Estimasi $etd</option>";
        }

    }

}
