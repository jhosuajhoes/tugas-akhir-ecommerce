<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password;

class RegisterController extends Controller
{
    public function index()
    {
        return view('customer.register',[

        ]);
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|max:255',
            'phone_number' => 'required|min:10|max:14',
            'email' => 'required|email:dns|unique:users',
            'address' => 'required|min:5|max:255',
            'password' => ['required',Password::min(8)->mixedCase()->letters()->numbers(),],
        ]);

        $validatedData['password'] = bcrypt($validatedData['password']);
        $validatedData['roles'] = 'customer';

        User::create($validatedData);
        return redirect('/login')->with('success','Registration Succesfull! Please Login');
    }
}
