<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function index()
    {
        return view('customer.login',[

        ]);
    }

    public function authenticate(Request $request)
    {
        $credentials= $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }
        return back()->with('loginError','Login Failed!');
    }
    public function logout()
    {
        request()->session()->flush();
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }

    public function forgotPassword()
    {
        return view('customer.forgot-password');
    }

    public function sendResetLink(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);

        $token = Str::random(64);
        DB::table('password_resets')->insert([
              'email'=>$request->email,
              'token'=>$token,
              'created_at'=>Carbon::now(),
        ]);
        $action_link = route('reset.password.form',['token'=>$token,'email'=>$request->email]);
        $body = "We have received a request to reset the password for <b>Skysea.co</b> account associated with ".$request->email.". You can reset your password by click link below.";

        Mail::send('customer.email-forgot',['action_link'=>$action_link,'body'=>$body], function($message) use ($request){
             $message->from('noreply@example.com', 'Skysea.co');
             $message->to($request->email, 'Username')
                     ->subject('Reset Password');
        });

        return back()->with('success', 'We have e-mailed your password reset link, please check in' . $request->email . 'inbox/spam');
    }

    public function showResetForm(Request $request, $token = null){
        return view('customer.reset-password')->with(['token'=>$token,'email'=>$request->email]);
    }

    public function resetPassword(Request $request){
        $request->validate([
             'email'=>'required|email|exists:users,email',
             'password'=>'required|min:5',
        ]);

        $check_token = DB::table('password_resets')->where([
             'email'=>$request->email,
             'token'=>$request->token,
        ])->first();

        if(!$check_token){
            return back()->withInput()->with('fail', 'Invalid token');
        }else{
            User::where('email', $request->email)->update([
                'password'=>bcrypt($request->password)
            ]);

            DB::table('password_resets')->where([
                'email'=>$request->email
            ])->delete();

            return redirect('/login')->with('success', 'Your password has been changed! You can login with new password');
        }
    }
}
