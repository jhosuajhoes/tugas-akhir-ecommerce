<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('customer.checkout',[
            'active' => 'shop',
            'categories' => Category::all(),
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'count_order' => Order::whereIn('unique_code',function($query){
                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','Ordered');
            })->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Cart')->get(),
            'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
            'qty_sum' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','Cart')->sum('quantity'),
            'total_harga' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','Cart')
                            ->sum('price_total'),
            'product' =>Product::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $stock = $request->stock;
        $price = $request->price;
        $qty = $request->quantity;
        $price_total = $price*$qty;

        if ($qty <= $stock) {
            $validatedData = $request->validate([
                'quantity' => 'required|max:255',
            ]);
            $validatedData['price_total'] = $price_total;

            Cart::where('id',$id)->update($validatedData);
            return redirect('/checkout')->with('toast_success', 'Berhasil update keranjang!');
        } else {
            Alert::warning('Out of Stock', 'Maaf jumlah yang diinput melebihi stok yang tersedia, mohon periksa kembali');
            return redirect('/checkout');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::find($id);
        $cart->delete($id);
        return redirect('/checkout')->with('toast_info', 'Barang telah dihapus dari keranjang!');
    }
}
