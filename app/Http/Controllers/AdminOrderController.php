<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;

class AdminOrderController extends Controller
{
    public function index()
    {
        $order = Order::all();
        return view('admin.order.index',[
            'active' => 'order',
            'orders' => $order
        ]);
    }

    public function order_details($unique_code)
    {
        return view('admin.order.order-details',[
            'active' => 'order',
            'order' => Order::where('unique_code',$unique_code)->first(),
            'order_cart' => Cart::where('unique_code',$unique_code)->where('status','Ordered')->get()
        ]);
    }

    public function order_invoice($unique_code)
    {
        return view('admin.order.order-invoice',[
            'order' => Order::where('unique_code',$unique_code)->first(),
            'order_cart' => Cart::where('unique_code',$unique_code)->where('status','Ordered')->get()
        ]);
    }

    public function update_status(Request $request, $unique_code)
    {
        // dd($request);
        $validatedData['status'] = $request->status;
        Order::where('unique_code',$unique_code)->update($validatedData);
        return redirect()->back()->with('toast_success', 'Order status updated!');
    }
}
