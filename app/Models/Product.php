<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    use HasFactory;

    protected $guarded = ['id'];


    public function color()
    {
        return $this->belongsTo(Color::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function size()
    {
        return $this->belongsTo(Size::class);
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

}
