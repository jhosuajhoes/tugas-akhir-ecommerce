<?php
namespace App;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ShipmentController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\AdminSizeController;
use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\ListOrderController;
use App\Http\Controllers\AdminColorController;
use App\Http\Controllers\AdminOrderController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\AdminProductController;
use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\AdminDashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class,'index']);
Route::get('/profile', [UserProfileController::class,'index']);
Route::post('/profile/{user:id}', [UserProfileController::class,'update']);
Route::delete('/profile-delete/{user:id}', [UserProfileController::class,'destroy']);
Route::get('/profile-security', [UserProfileController::class,'security']);
Route::post('/profile-security', [UserProfileController::class,'change_password']);
Route::get('/about', [Controller::class,'about']);
Route::get('/order', [ListOrderController::class,'index'])->middleware('auth');
Route::get('/order-details/{order:unique_code}', [ListOrderController::class,'order_details'])->middleware('auth');
Route::get('/order-invoice-print/{order:unique_code}', [ListOrderController::class,'order_invoice'])->middleware('auth');
Route::get('/shop',[ShopController::class, 'show']);
Route::get('/shop/{category}',[ShopController::class, 'category_filter']);
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);
Route::get('/forgot-password', [LoginController::class, 'forgotPassword'])->name('forgot.password.form');
Route::post('/forgot-password', [LoginController::class, 'sendResetLink'])->name('forgot.password.link');
Route::get('/reset-password/{token}',[LoginController::class,'showResetForm'])->name('reset.password.form');
Route::post('/reset-password',[LoginController::class,'resetPassword'])->name('reset.password');


Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);
Route::get('/product/{product:id}', [ProductController::class, 'showProduct']);
Route::post('/product', [ProductController::class, 'storeProduct'])->middleware('auth');
Route::get('/checkout/shipment', [ShipmentController::class,'show'])->middleware('auth');
Route::post('/product/wishlist', [WishlistController::class, 'storeWishlist'])->middleware('auth');
Route::resource('/wishlist', WishlistController::class)->middleware('auth');
Route::resource('/checkout', CheckoutController::class)->middleware('auth');

/* ajax route */
Route::post('/getData/ajax', [ShipmentController::class,'show'])->name('getData');
Route::post('/getCity/ajax/', [ShipmentController::class,'ajax'])->name('getCities');
Route::post('/getCourier/ajax/', [ShipmentController::class,'courier'])->name('getCourier');
Route::post('/getEstimation/ajax/', [ShipmentController::class,'estimation'])->name('getEstimation');
Route::post('/getStock/ajax/', [ProductController::class,'get_stock'])->name('getStock');

Route::post('/payment', [PaymentController::class,'show'])->middleware('auth');
Route::post('/payment/post', [PaymentController::class,'payment'])->middleware('auth');

/* admin route  */
Route::get('/admin/dashboard',[AdminDashboardController::class,'index'])->middleware('auth');
Route::get('/admin/print-transaction-report',[AdminDashboardController::class,'print_transactions'])->middleware('auth');
Route::post('/admin/export_excel', [AdminDashboardController::class,'export_excel']);
Route::post('/admin/export_csv', [AdminDashboardController::class,'export_csv']);
Route::post('/admin/export_pdf', [AdminDashboardController::class,'export_pdf']);
Route::get('/admin/order',[AdminOrderController::class,'index'])->middleware('auth');
Route::get('/admin/order-details/{order:unique_code}',[AdminOrderController::class,'order_details'])->middleware('auth');
Route::post('/admin/order-details/{order:unique_code}',[AdminOrderController::class,'update_status'])->middleware('auth');
Route::get('/admin/order-invoice/{order:unique_code}',[AdminOrderController::class,'order_invoice'])->middleware('auth');

Route::get('/admin/product/{product:name}/edit-all', [AdminProductController::class,'edit_all'])->middleware('auth');
Route::get('/admin/product-show/{product:name}', [AdminProductController::class,'index_product'])->middleware('auth');
Route::post('/admin/product/create-variant', [AdminProductController::class,'store_product'])->middleware('auth');
Route::put('/admin/product-update/{product:name}', [AdminProductController::class,'update_all'])->middleware('auth');
Route::delete('/admin/product-delete/{product:name}', [AdminProductController::class,'destroy_all'])->middleware('auth');
Route::resource('/admin/product',AdminProductController::class)->middleware('auth');
Route::resource('/admin/category',AdminCategoryController::class)->middleware('auth');
Route::resource('/admin/color',AdminColorController::class)->middleware('auth');
Route::resource('/admin/size',AdminSizeController::class)->middleware('auth');
Route::get('/admin/product/{product:id}/image-change',[AdminProductController::class,'image_change'])->middleware('auth');
Route::post('/admin/product/{product:id}/image-change',[AdminProductController::class,'image_store'])->middleware('auth');
Route::get('/admin/user/customer', [AdminUserController::class,'customer'])->middleware('auth');
Route::get('/admin/user/admin', [AdminUserController::class,'admin'])->middleware('auth');
Route::get('/admin/user/create-admin', [AdminUserController::class,'create_admin'])->middleware('auth');
Route::post('/admin/user/create-admin', [AdminUserController::class,'store_admin'])->middleware('auth');



