<!-- BEGIN: Main Menu-->
<div class="horizontal-menu-wrapper">
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-shadow menu-border container-xxl" role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item me-auto"><a class="navbar-brand" href="/"><span class="brand-logo">
                        <h2 class="brand-text mb-0">Skysea.co</h2>
                    </a>
                </li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <!-- include includes/mixins-->
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item {{($active === "home")?'active' : ''}}"><a class="nav-link d-flex align-items-center" href="/"><i data-feather="home"></i><span data-i18n="Dashboards">Home</span></a>
                </li>
                <li class="nav-item {{($active === "shop")?'active' : ''}}"><a class="nav-link d-flex align-items-center" href="/shop"><i data-feather="shopping-cart"></i><span data-i18n="Forms &amp; Tables">Shop</span></a>
                </li>
                <li class="dropdown nav-item {{($active === "category")?'active' : ''}}" data-menu="dropdown"><a class="nav-link d-flex align-items-center" href="#"><i data-feather="shopping-bag"></i><span data-i18n="User Interface">Category</span></a>
                    <ul class="dropdown-menu" data-bs-popper="none">
                        @foreach ($categories as $category)
                        <li data-menu=""><a class="dropdown-item d-flex align-items-center" href="/shop/{{$category->name}}" data-bs-toggle="" data-i18n="Typography"><i data-feather='chevron-right'></i></i><span data-i18n="Typography">{{$category->name}}</span></a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @auth
                <li class="nav-item {{($active === "listorder")?'active' : ''}}"><a class="nav-link d-flex align-items-center" href="/order"><i data-feather="edit"></i><span data-i18n="Forms &amp; Tables">Order List</span></a>
                </li>
                @endauth
                {{-- <li class="dropdown nav-item" data-menu="dropdown"><a class="nav-link d-flex align-items-center" href="#" data-bs-toggle="dropdown"><i data-feather="book-open"></i><span data-i18n="Forms &amp; Tables">How to Buy?</span></a> --}}
                </li>
                <li class="nav-item {{($active === "about")?'active' : ''}}"><a class="nav-link d-flex align-items-center" href="/about"><i data-feather="info"></i><span data-i18n="Forms &amp; Tables">About Us</span></a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- END: Main Menu-->
