<!-- Footer -->
<div class="row col-12">

</div>
<footer class="mt-5 shadow bg-white text-muted">
    <!-- Section: Social media -->
    <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
        <!-- Left -->
        <div class="me-5 d-none d-lg-block">
            <span>Get connected with us on social networks:</span>
        </div>
        <!-- Left -->

        <!-- Right -->
        <div>
            <a href="mailto:https://shutx2306@gmail.com" class="me-4 text-reset">
                <i class="bi bi-envelope" style="font-size: 25px;"></i>
            </a>
            <a href="https://www.instagram.com/skysea.co/" target="_blank" class="me-4 text-reset">
                <i class="bi bi-instagram" style="font-size: 25px;"></i>
            </a>
            <a href="https://wa.me/6282254168212" target="_blank" class="me-4 text-reset">
                <i class="bi bi-whatsapp" style="font-size: 24px;"></i>
            </a>
        </div>
        <!-- Right -->
    </section>
    <!-- Section: Social media -->

    <!-- Section: Links  -->
    <section>
        <div class="text-md-start p-2 mt-5">
            <!-- Grid row -->
            <div class="row mt-3">
                <!-- Grid column -->
                <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4 ">
                    <!-- Content -->
                    <h6 class="text-uppercase fw-bold mb-4">
                        Skysea.co
                    </h6>
                    <p>
                        Skysea.co adalah sebuah online shop yang mulai berdiri pada tanggal 28 maret 2019. Fashioninshop
                        itu sendiri adalah nama yang diambil dari kata Bahasa inggris fashion yang berarti gaya
                        berpakaian dan kata “in” merupakan nama dari owner yaitu Iin. Fashioninshop menjual produk
                        berupa hijab dan outfit kekinian.
                    </p>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                    <!-- Links -->
                    <h6 class="text-uppercase fw-bold mb-4">
                        Products
                    </h6>
                    <p>
                        <a href="/shop/T-Shirt" class="text-reset">T-Shirt</a>
                    </p>
                    <p>
                        <a href="/shop/Pants" class="text-reset">Pants</a>
                    </p>
                </div>
                <!-- Grid column -->


                <!-- Grid column -->
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                    <!-- Links -->
                    <h6 class="text-uppercase fw-bold mb-4">
                        Contact
                    </h6>
                    <p><i data-feather='map-pin'></i> Skysea.co, Yogyakarta.</p>
                    <p>
                        <i data-feather='mail'></i>
                        skysea@gmail.com
                    </p>
                    <p><i data-feather='phone'></i> +62 8113 2322</p>
                </div>
                <!-- Grid column -->
            </div>
            <!-- Grid row -->
        </div>
    </section>
    <!-- Section: Links  -->
</footer>
<!-- Copyright -->
<div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
    © 2021 Copyright:
    <a class="text-reset fw-bold" href="#!">Jhosua Matindas</a>
</div>
<!-- Copyright -->

<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>

<!-- Footer -->
