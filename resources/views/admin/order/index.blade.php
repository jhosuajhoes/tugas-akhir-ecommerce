@extends('admin.admin-layouts.app')
@section('content')
<section class="invoice-list-wrapper">
    <div class="card">
        <div class="card-datatable table-responsive">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-center">
                    <h2>Order</h2>
                </div>
                <hr />
                <div class="data-tables datatable-dark">
                    <table id="orderTable" class="datatables-basic table" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Order Code</th>
                                <th>Customer Name</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                            <tr>
                                <td>
                                    {{$loop->iteration}}
                                </td>
                                <td>
                                    <a style="font-weight: 500" href="/admin/order-details/{{$order->unique_code}}">
                                        #{{$order->unique_code}}
                                    </a>
                                </td>
                                <td>{{$order->cart->user->name}}</td>
                                <td>{{$order->date}}</td>
                                <td>{{$order->gross_amount}}</td>
                                @if($order->status == 'settlement')
                                <td><span class="badge rounded-pill badge-light-primary me-1">paid</span></td>
                                @elseif ($order->status == 'delivered')
                                <td><span class="badge rounded-pill badge-light-success me-1">delivered</span></td>
                                @else
                                <td><span class="badge rounded-pill badge-light-info me-1">{{$order->status}}</span></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Order Code</th>
                                <th>Customer Name</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection
