@extends('admin.admin-layouts.app')
@section('content')
<!-- BEGIN: Content-->
<section class="invoice-preview-wrapper">
    <div class="row invoice-preview">
        <!-- Invoice -->
        <div class="col-xl-9 col-md-8 col-12">
            <div class="card invoice-preview-card">
                <div class="card-body invoice-padding pb-0">
                    <!-- Header starts -->
                    <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                        <div>
                            <div class="logo-wrapper">
                                <h3 class="text-primary invoice-logo">Skysea.co</h3>
                            </div>
                            <p class="card-text mb-25">Pogung Lor Office, Sinduadi, Sleman</p>
                            <p class="card-text mb-25">Yogyakarta, 55284, Indonesia</p>
                            <p class="card-text mb-0">+62 822 541 682 12</p>
                        </div>
                        <div class="mt-md-0 mt-2">
                            <h4 class="invoice-title">
                                Invoice
                                <span class="invoice-number">#{{$order->unique_code}}</span>
                            </h4>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Date Order:</p>
                                <p class="invoice-date">{{$order->date}}</p>
                            </div>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Status:</p>
                                <p class="invoice-date">{{$order->status}}</p>
                            </div>
                        </div>
                    </div>
                    <!-- Header ends -->
                </div>

                <hr class="invoice-spacing" />

                <!-- Address and Contact starts -->
                <div class="card-body invoice-padding pt-0">
                    <div class="row invoice-spacing">
                        <div class="col-xl-8 p-0">
                            <h6 class="mb-2">Invoice To:</h6>
                            <h6 class="mb-25">{{$order->cart->user->name}}</h6>
                            <p class="card-text mb-25">Customer</p>
                            <p class="card-text mb-25">{{$order->cart->user->address}}</p>
                            <p class="card-text mb-25">{{$order->cart->user->phone_number}}</p>
                            <p class="card-text mb-0">{{$order->cart->user->email}}</p>
                        </div>
                        <div class="col-xl-4 p-0 mt-xl-0 mt-2">
                            <h6 class="mb-2">Payment Details:</h6>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="pe-1">Total Due:</td>
                                        <td><span class="fw-bold">{{$order->payment->gross_amount}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">Payment:</td>
                                        <td>{{$order->payment->payment_type}}</td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">Country:</td>
                                        <td>Indonesia</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Address and Contact ends -->

                <!-- Invoice Description starts -->
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="py-1">Product description</th>
                                <th class="py-1">Color</th>
                                <th class="py-1">Size</th>
                                <th class="py-1">Qty</th>
                                <th class="py-1">Price</th>
                                <th class="py-1">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order_cart as $result)
                            <tr class="border-bottom">
                                <td class="py-1">
                                    <p class="card-text fw-bold mb-25">{{$result->product->get(0)->name}}</p>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->product->get(0)->color->name}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->product->get(0)->size->name}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->quantity}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->product->get(0)->price}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->quantity * $result->product->get(0)->price}}</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-body invoice-padding pb-0">
                    <div class="row invoice-sales-total-wrapper">
                        <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
                            <p class="card-text mb-0">
                                <span class="fw-bold">Admin:</span> <span class="ms-75">{{auth()->user()->name}}</span>
                            </p>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
                            <div class="invoice-total-wrapper">
                                <div class="invoice-total-item">
                                    <p class="invoice-total-title">Courier:</p>
                                    <p class="invoice-total-amount">{{$order->shipment->courier}}</p>
                                </div>
                                <div class="invoice-total-item">
                                    <p class="invoice-total-title">Shipping cost:</p>
                                    <p class="invoice-total-title text-warning">{{$order->shipment->shipping_cost}}</p>
                                </div>
                                <hr class="my-50" />
                                <div class="invoice-total-item">
                                    <p class="invoice-total-title">Total:</p>
                                    <p class="invoice-total-amount">{{$order->payment->gross_amount}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Invoice Description ends -->

                <hr class="invoice-spacing" />

                <!-- Invoice Note starts -->
                <div class="card-body invoice-padding pt-0">
                    <div class="row">
                        <div class="col-12">
                            <span class="fw-bold">Note:</span>
                            <span>It was a pleasure working with you and your team. We hope you will keep us in mind
                                for future freelance
                                projects. Thank You!</span>
                        </div>
                    </div>
                </div>
                <!-- Invoice Note ends -->
            </div>
        </div>
        <!-- /Invoice -->

        <!-- Invoice Actions -->
        <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
            <div class="card">
                <div class="card-body">
                    @if ($order->status == 'settlement')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        <input type="hidden" name="status" value="in delivery">
                        <button type="submit" class="btn btn-primary w-100 mb-75">
                            Set to Delivery
                        </button>
                    </form>
                    @elseif ($order->status == 'in delivery')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        <input type="hidden" name="status" value="delivered">
                        <button type="submit" class="btn btn-secondary w-100 mb-75">
                            Set to Done
                        </button>
                    </form>
                    @elseif ($order->status == 'pending')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        <input type="hidden" name="status" value="canceled">
                        <button type="submit" class="btn btn-danger w-100 mb-75">
                           Cancel order
                        </button>
                    </form>
                    @elseif ($order->status == 'delivered')
                    <span class="badge rounded-pill badge-light-success me-1 mt-1 mb-2">Order has been completed</span>
                    @else
                    <a href="#" class="btn btn-warning w-100 btn-download-invoice mb-75">Order canceled!</a>
                    @endif
                    <a class="btn btn-outline-secondary w-100 mb-75" href="/admin/order-invoice/{{$order->unique_code}}"
                        target="_blank">
                        Print </a>
                </div>
            </div>
        </div>
        <!-- /Invoice Actions -->
    </div>
</section>

<!-- Send Invoice Sidebar -->
<div class="modal modal-slide-in fade" id="send-invoice-sidebar" aria-hidden="true">
    <div class="modal-dialog sidebar-lg">
        <div class="modal-content p-0">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
            <div class="modal-header mb-1">
                <h5 class="modal-title">
                    <span class="align-middle">Send Invoice</span>
                </h5>
            </div>
            <div class="modal-body flex-grow-1">
                <form>
                    <div class="mb-1">
                        <label for="invoice-from" class="form-label">From</label>
                        <input type="text" class="form-control" id="invoice-from" value="shelbyComapny@email.com"
                            placeholder="company@email.com" />
                    </div>
                    <div class="mb-1">
                        <label for="invoice-to" class="form-label">To</label>
                        <input type="text" class="form-control" id="invoice-to" value="qConsolidated@email.com"
                            placeholder="company@email.com" />
                    </div>
                    <div class="mb-1">
                        <label for="invoice-subject" class="form-label">Subject</label>
                        <input type="text" class="form-control" id="invoice-subject"
                            value="Invoice of purchased Admin Templates" placeholder="Invoice regarding goods" />
                    </div>
                    <div class="mb-1">
                        <label for="invoice-message" class="form-label">Message</label>
                        <textarea class="form-control" name="invoice-message" id="invoice-message" cols="3" rows="11"
                            placeholder="Message...">
Dear Queen Consolidated,

Thank you for your business, always a pleasure to work with you!

We have generated a new invoice in the amount of $95.59

We would appreciate payment of this invoice by 05/11/2019</textarea>
                    </div>
                    <div class="mb-1">
                        <span class="badge badge-light-primary">
                            <i data-feather="link" class="me-25"></i>
                            <span class="align-middle">Invoice Attached</span>
                        </span>
                    </div>
                    <div class="mb-1 d-flex flex-wrap mt-2">
                        <button type="button" class="btn btn-primary me-1" data-bs-dismiss="modal">Send</button>
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Send Invoice Sidebar -->

<!-- Add Payment Sidebar -->
<div class="modal modal-slide-in fade" id="add-payment-sidebar" aria-hidden="true">
    <div class="modal-dialog sidebar-lg">
        <div class="modal-content p-0">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
            <div class="modal-header mb-1">
                <h5 class="modal-title">
                    <span class="align-middle">Add Payment</span>
                </h5>
            </div>
            <div class="modal-body flex-grow-1">
                <form>
                    <div class="mb-1">
                        <input id="balance" class="form-control" type="text" value="Invoice Balance: 5000.00"
                            disabled />
                    </div>
                    <div class="mb-1">
                        <label class="form-label" for="amount">Payment Amount</label>
                        <input id="amount" class="form-control" type="number" placeholder="$1000" />
                    </div>
                    <div class="mb-1">
                        <label class="form-label" for="payment-date">Payment Date</label>
                        <input id="payment-date" class="form-control date-picker" type="text" />
                    </div>
                    <div class="mb-1">
                        <label class="form-label" for="payment-method">Payment Method</label>
                        <select class="form-select" id="payment-method">
                            <option value="" selected disabled>Select payment method</option>
                            <option value="Cash">Cash</option>
                            <option value="Bank Transfer">Bank Transfer</option>
                            <option value="Debit">Debit</option>
                            <option value="Credit">Credit</option>
                            <option value="Paypal">Paypal</option>
                        </select>
                    </div>
                    <div class="mb-1">
                        <label class="form-label" for="payment-note">Internal Payment Note</label>
                        <textarea class="form-control" id="payment-note" rows="5"
                            placeholder="Internal Payment Note"></textarea>
                    </div>
                    <div class="d-flex flex-wrap mb-0">
                        <button type="button" class="btn btn-primary me-1" data-bs-dismiss="modal">Send</button>
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END: Content-->

@endsection
