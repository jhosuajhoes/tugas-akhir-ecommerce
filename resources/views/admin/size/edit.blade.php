@extends('admin.admin-layouts.app')
@section('content')
<div class="content-body">
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="card">
                    <form method="post" action="/admin/size/{{$size->id}}" >
                    @method('put')
                    @csrf
                    <div class="card-header">
                        <h4 class="card-title">Edit Size</h4>
                    </div>
                    <div class="card-body">
                        <div class="mb-2">
                            <label class="form-label" name="sickness"  for="sickness">Size name</label>
                                <input type="text" name="name" class="form-control mt-1 @error('name') is-invalid @enderror" placeholder="Size" value="{{old('sickness',$size->name)}}"/>
                                @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary me-1">Update</button>
                        <a href="/admin/size" class="btn btn-outline-secondary">Back</a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
</div>
@endsection
