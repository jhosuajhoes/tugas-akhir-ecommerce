@extends('admin.admin-layouts.app')
@section('content')
<section class="invoice-list-wrapper">
    <div class="card">
        <div class="card-datatable table-responsive">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-center">
                    <h2>Product List</h2>
                    <a href="/admin/product/create" class="btn btn-primary">
                        Input Data
                    </a>
                </div>
                <hr />
                <div class="data-tables datatable-dark">
                    <table id="orderTable" class="datatables-basic table" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Product</th>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <div class="boxImage">
                                        <a class="linkk" href="/admin/product/{{$product->id}}/image-change">
                                            <img class="img-fluid" width="80px" height="80px"
                                                src="{{ asset('storage/' . $product->image)}}"><br>
                                            <i class="iconsw overlayy" data-feather='edit-2'></i>
                                        </a>
                                    </div>
                                </td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->color->name}}</td>
                                <td>{{$product->size->name}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->stock}}</td>
                                <td>
                                    <form method="post" action="/admin/product/{{$product->id}}">
                                        <a href="/admin/product/{{$product->id}}" class="badge bg-info"><span
                                                data-feather="eye"></span></a>
                                        <a href="/admin/product/{{$product->id}}/edit" class="badge bg-warning"><span
                                                data-feather="edit"></span></a>
                                        @method('delete')
                                        @csrf
                                        <button class="badge bg-danger border-0"
                                            onclick="return confirm('Delete data?')">
                                            <span data-feather="x-circle"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Product</th>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


@endsection
