@extends('admin.admin-layouts.app')
@section('content')
<div class="content-body">
    <section id="description-list-alignment">
        <div class="row match-height">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Product info <small class="text-muted">details</small></h4>
                    </div>
                    <div class="card-body">
                        <div class="row p-1">
                            <div class="col-sm-8 col-12">
                                <dl class="row">
                                    <dt class="col-sm-3">Product name</dt>
                                    <dd class="col-sm-9">{{$product->name}}</dd>
                                </dl>
                                <dl class="row">
                                    <dt class="col-sm-3">Category</dt>
                                    <dd class="col-sm-9">{{$product->category->name}}</dd>
                                </dl>
                                <dl class="row">
                                    <dt class="col-sm-3">Size</dt>
                                    <dd class="col-sm-9">{{$product->size->name}}</dd>
                                </dl>
                                <dl class="row">
                                    <dt class="col-sm-3">Stock</dt>
                                    <dd class="col-sm-9">{{$product->stock}}</dd>
                                </dl>
                                <dl class="row">
                                    <dt class="col-sm-3">Color</dt>
                                    <dd class="col-sm-9">{{$product->color->name}}</dd>
                                </dl>
                                <dl class="row">
                                    <dt class="col-sm-3 text-truncate">Description</dt>
                                    <dd class="col-sm-9">
                                        {{$product->description}}
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-sm-4 col-12">
                                <img class="img-fluid" id="imgInput" src="{{ asset('storage/' . $product->image)}}"
                                    alt="Preview Image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
</div>
@endsection
