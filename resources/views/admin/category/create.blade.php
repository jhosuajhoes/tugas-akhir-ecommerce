@extends('admin.admin-layouts.app')
@section('content')
<div class="content-body">
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-6 col-12">
                <section id="input-group-basic" class="basic-select2">
                    <div class="col-md-12">
                        <div class="card">
                            <form method="post" action="/admin/category">
                                @csrf
                                <div class="card-header">
                                    <h4 class="card-title">Insert New Category</h4>
                                </div>
                                <div class="card-body">
                                    <div class="input-group mb-2">
                                        <input type="text" name="name"
                                            class="form-control @error('name') is-invalid @enderror"
                                            placeholder="Category" value="{{old('name')}}" />
                                        @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary me-1">Insert</button>
                                    <a href="/admin/category" class="btn btn-outline-secondary">Back</a>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
    </section>
</div>
</div>
</section>
<!-- Basic Horizontal form layout section end -->
</div>
@endsection
