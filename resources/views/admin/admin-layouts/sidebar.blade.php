
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="/home">
                        <span class="brand-logo">
                            <img src="{{ asset('img/village.png') }}" alt="">
                        </span>
                    <h2 class="brand-text">Skysea.co</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item {{($active === "dashboard")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/dashboard"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Home">Dashboard</span></a>
            </li>
            <hr />
            <li class="nav-item {{($active === "order")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/order"><i data-feather='file-text'></i><span class="menu-title text-truncate" data-i18n="Page Layouts">Manage Order</span></a>
            </li>
            <hr />
            <li class="nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='database'></i><span class="menu-title text-truncate" data-i18n="Page Layouts">Manage Data</span></a>
                <ul class="menu-content">
                    <li class="{{($active === "product")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/product"><i data-feather="circle"></i><span class="menu-item text-truncate">Product</span></a>
                    </li>
                    <li class="{{($active === "category")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/category"><i data-feather="circle"></i><span class="menu-item text-truncate">Category</span></a>
                    </li>
                    <li class="{{($active === "color")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/color"><i data-feather="circle"></i><span class="menu-item text-truncate">Color</span></a>
                    </li><li class="{{($active === "size")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/size"><i data-feather="circle"></i><span class="menu-item text-truncate">Size</span></a>
                    </li>
                </ul>
            </li>
            <hr />
            <li class="nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='users'></i><span class="menu-title text-truncate" data-i18n="Page Layouts">Manage User</span></a>
                <ul class="menu-content">
                    <li class="{{($active === "admin")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/user/admin"><i data-feather="circle"></i><span class="menu-item text-truncate">Admin</span></a>
                    </li>
                    <li class="{{($active === "customer")?'active' : ''}}"><a class="d-flex align-items-center" href="/admin/user/customer"><i data-feather="circle"></i><span class="menu-item text-truncate">Customer</span></a>
                    </li>
                </ul>
            </li>
            <hr />
            <li class="nav-item"><a class="d-flex align-items-center" href="/"><i data-feather='shopping-cart'></i><span class="menu-title text-truncate" data-i18n="Page Layouts">Back to Shop</span></a>
            </li>
            <hr />
        </ul>
    </div>
</div>

