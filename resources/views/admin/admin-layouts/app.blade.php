<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description"
        content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Admin - Skysea.co</title>
    @include('admin.admin-layouts.FileVendorCSS')

    <script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>


    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')}}">


    {{-- Custom CSS --}}
    <style>
        /* css untuk icon ubah gambar produk */
        /* Container diperlukan untuk memposisikan overlay. Sesuaikan lebarnya sesuai dengan kebutuhan*/
        .boxImage {
            position: relative;
        }

        /* Efek overlay (tinggi dan lebar penuh) - diletakkan di atas containernya dan di atas gambar */
        .overlayy {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 30%;
            width: 30%;
            opacity: 0;
            background-color: rgb(255, 255, 255)23);
        }

        /* Saat akan mengarahkan mouse ke container, fade di ikon overlay*/
        .boxImage:hover .overlayy {
            opacity: 1;
        }

        /* Ikon di dalam overlay diposisikan di tengah secara vertikal dan horizontal */
        .iconsw {
            color: #FFC107;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        /* Saat kita menggerakkan mouse ke atas ikon, ubah warna */
        .linkk :hover {
            color: #FFC107;
        }

    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click"
    data-menu="vertical-menu-modern" data-col="">

    @include('sweetalert::alert')

    @include('admin.admin-layouts.header')

    @include('admin.admin-layouts.sidebar')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">

                @yield('content')



            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    @include('admin.admin-layouts.footer')

    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('themes/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('themes/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/pickers/pickadate/legacy.js')}}"></script>


    <!-- BEGIN: Datatables Vendor JS-->
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/responsive.bootstrap5.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/jszip.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js')}}"></script>
    <!-- END: Datatables Vendor JS-->

    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('themes/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->
    <!-- choose one -->
    <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>
    <!-- BEGIN: Page JS-->
    <script src="{{asset('themes/app-assets/js/scripts/forms/pickers/form-pickers.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/forms/form-select2.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/forms/form-repeater.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/pages/app-invoice.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/extensions/ext-component-sweet-alerts.js')}}"></script>
    <script>
        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })

        $(document).ready(function () {
            $('#orderTable').DataTable();
        });

        var loadFile = function (event) {
            var output = document.getElementById('imgInput');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function () {
                URL.revokeObjectURL(output.src) // free memory
            }
        };

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.0/chart.min.js"></script>
</body>
<!-- END: Body-->

</html>
