@extends('admin.admin-layouts.app')
@section('content')
<section class="invoice-list-wrapper">
    <div class="card">
        <div class="card-datatable table-responsive">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-center">
                    <h2>Product Color</h2>
                    <a class="btn btn-primary" href="/admin/color/create" role="button">Input Data</a>
                </div>
                <hr />
                <div class="data-tables datatable-dark">
                    <table id="orderTable" class="datatables-basic table" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Product in color</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($colors as $color)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$color->name}}</td>
                                <td>{{$product->where('color_id',$color->id)->count()}}</td>
                                <td>
                                    <form method="post" action="/admin/color/{{$color->id}}">
                                    <a href="/admin/color/{{$color->id}}/edit" class="badge bg-warning"><span
                                            data-feather="edit"></span></a>
                                        @method('delete')
                                        @csrf
                                        <button class="badge bg-danger border-0"
                                            onclick="return confirm('Delete data?')">
                                            <span data-feather="x-circle"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection
