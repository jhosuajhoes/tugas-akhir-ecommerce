@extends('admin.admin-layouts.app-reports')
@section('content')
<div class="container">
    <table class="table table-sm" id="example">
        <thead class="thead-dark">
            <tr>
                <td colspan="11">Skysea Order Reports</td>
            </tr>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Order ID</th>
                <th scope="col">Transaction ID</th>
                <th scope="col">Customer Name</th>
                <th scope="col">Date</th>
                <th scope="col">Product Name</th>
                <th scope="col">Color</th>
                <th scope="col">Size</th>
                <th scope="col">Qty</th>
                <th scope="col">Total</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td><strong>{{$order->unique_code}}</strong>
                </td>
                <td>{{$order->transaction_id}}</td>
                <td>{{$order->cart->user->name}}</td>
                <td>{{$order->date}}</td>
                <td>{{$order->cart->product->get(0)->name}}</td>
                <td>{{$order->cart->product->get(0)->color->name}}</td>
                <td>{{$order->cart->product->get(0)->size->name}}</td>
                <td>{{$order->cart->quantity}}</td>
                <td>{{$order->gross_amount}}</td>
                <td>{{$order->status}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
