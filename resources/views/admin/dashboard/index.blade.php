@extends('admin.admin-layouts.app')

@section('content')
<div class="content-body">
    <div class="row">
        <div class="col-12">
            <!-- Dashboard Ecommerce Starts -->
            <section id="dashboard-ecommerce">
                <div class="row match-height">
                    <!-- Medal Card -->
                    <div class="col-xl-4 col-md-6 col-12">
                        <div class="card card-congratulation-medal">
                            <div class="card-body">
                                <h5>Welcome's {{auth()->user()->name}}</h5>
                                <p class="card-text font-small-3">Your Total Incomes</p>
                                <h3 class="mb-75 mt-2 pt-50">
                                    <a href="#">IDR{{$incomes}}k</a>
                                </h3>
                                <a href="/admin/order" class="btn btn-primary">View Sales</a>
                            </div>
                        </div>
                    </div>
                    <!--/ Medal Card -->

                    <!-- Statistics Card -->
                    <div class="col-xl-8 col-md-6 col-12">
                        <div class="card card-statistics">
                            <div class="card-header">
                                <h4 class="card-title">Statistics</h4>
                                <div class="d-flex align-items-center">
                                    <p class="card-text font-small-2 me-25 mb-0">Updated</p>
                                </div>
                            </div>
                            <div class="card-body statistics-body">
                                <div class="row">
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-success me-2">
                                                <div class="avatar-content">
                                                    <i data-feather='shopping-cart' class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$total_order}}</h4>
                                                <p class="card-text font-small-3 mb-0">Orders</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-info me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="user" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$customers_amount}}</h4>
                                                <p class="card-text font-small-3 mb-0">Customers</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-danger me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="box" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$products_amount}}</h4>
                                                <p class="card-text font-small-3 mb-0">Stocks</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-primary me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="trending-up" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$sold_quantity}}</h4>
                                                <p class="card-text font-small-3 mb-0">Sold</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Statistics Card -->
                </div>

                <div class="row match-height">
                    <!-- Transaction Card -->
                    <form action="/admin/print-transaction-report" target="_blank" method="GET">
                        <div class="col-lg-6 col-12">
                            <div class="card card-transaction">
                                <div class="card-header">
                                    <h4 class="card-title">Print Transactions Report</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 mb-1">
                                            <label class="form-label" for="fp-default">Start</label>
                                            <input type="date" name="date_start" id="fp-default" class="form-control"
                                                placeholder="YYYY-MM-DD" required/>
                                        </div>
                                        <div class="col-md-12 mb-1">
                                            <label class="form-label" for="fp-default">End</label>
                                            <input type="date" name="date_end" id="fp-default" class="form-control"
                                                placeholder="YYYY-MM-DD" required/>
                                        </div>
                                        <div class="col-md-12 mb-1">
                                            <label class="form-label" for="fp-default">Transaction Status</label>
                                            <select class="form-select" name="status" id="status" required>
                                                <option value="" holder>- Select Status -</option>
                                                <option value="All">All</option>
                                                <option value="delivered">Done (Delivered)</option>
                                                <option value="settlement">Paid</option>
                                                <option value="in delivery">In Delivery</option>
                                                <option value="pending">Pending</option>
                                                <option value="cancel">Cancel</option>
                                            </select>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-info"><span><i
                                                data-feather='printer'></i></span> Print</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--/ Transaction Card -->
                </div>
            </section>
            <!-- Dashboard Ecommerce ends -->
        </div>
    </div>
</div>
@endsection
