@extends('partial.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="card">
                    <div class="card-header justify-content-center border-bottom">
                        <h4 class="card-title">Details Order</h4>
                    </div>
                    <div class="card-body py-1">
                        <div class="row g-1">
                            <div class="col-12 col-md-4">
                                <h6 class="mb-2">Customer:</h6>
                                <h6 class="mb-25">{{$data_request->name}}</h6>
                                <p class="card-text mb-25">{{$data_request->phone_number}}</p>
                                <p class="card-text mb-25">{{$data_request->email}}</p>
                                <p class="card-text mb-0">{{$data_request->address}}</p>
                            </div>
                            <div class="col-12 col-md-4">
                                <h6 class="mb-2">Shipping details:</h6>
                                <p class="card-text mb-25">{{$city->city_name}}, {{$city->province->province}}</p>
                                <p class="card-text text-uppercase mb-25">jne</p>
                                <p class="card-text mb-25">{{$data_request->address}}</p>
                                <p class="card-text mb-25">Estimated Arrival at {{$estimasi}}</p>
                                <h6 class="text-success mb-0">Cost : Rp,{{$data_request->ongkir}}</h6>
                            </div>
                            <div class="col-12 col-md-4">
                                <h6 class="mb-2">Your Order: <a href="#" class="font-weight-bold">#{{$unique_code}}</a></h6>
                                <div class="table-responsive">
                                    <table class="table-sm fs-13">
                                        <tbody>
                                            @foreach ($checkouts as $checkout)
                                            <tr>
                                                <td>{{$checkout->product->get(0)->name}} -
                                                    {{$checkout->product->get(0)->color->name}},{{$checkout->product->get(0)->size->name}}
                                                </td>
                                                <td>{{$checkout->quantity}}</td>
                                                <td>Rp{{ $checkout->quantity * $checkout->product->get(0)->price }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <h4 class="py-1">Total : Rp,{{$data_request->total_bayar}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="alert alert-warning">
                            <h4 class="alert-heading">Please check your Order before pay.</h4>
                            <div class="alert-body fw-normal">
                                Once you click pay button, you will be redirect to payment method.
                            </div>
                        </div>
                        <button type="submit" id="pay-button" class="btn btn-warning">Pay Now</button>
                        <form action="/payment/post" id="submit_form" method="POST">
                            @csrf
                            <input type="hidden" name="json" id="json_callback">
                            <input type="hidden" name="unique_code" value="{{$unique_code}}">
                            <input type="hidden" name="courier" value="{{$data_request->courier}}">
                            <input type="hidden" name="estimation" value="{{$data_request->estimasi}}">
                            <input type="hidden" name="shipment_cost" value="{{$data_request->ongkir}}">
                            <input type="hidden" name="address" value="{{$data_request->address}}">
                            <input type="hidden" name="city" value="{{$data_request->destination}}">
                        @foreach ($checkouts as $checkout)
                            <input type="hidden" name="qty[]" value="{{$checkout->quantity}}">
                            <input type="hidden" name="stock[]" value="{{$checkout->product->get(0)->stock}}">
                            <input type="hidden" name="product_id[]" value="{{$checkout->product->get(0)->id}}">
                        @endforeach
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    // For example trigger on button clicked, or any time you need
    var payButton = document.getElementById('pay-button');
    payButton.addEventListener('click', function () {
        // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
        window.snap.pay("{{$snap_token}}", {
            onSuccess: function (result) {
                /* You may add your own implementation here */
                // alert("payment success!");
                console.log(result);
                send_respon_to_form(result);
            },
            onPending: function (result) {
                /* You may add your own implementation here */
                // alert("wating your payment!");
                console.log(result);
                send_respon_to_form(result);
            },
            onError: function (result) {
                /* You may add your own implementation here */
                alert("payment failed!");
                console.log(result);
                send_respon_to_form(result);
            },
            onClose: function () {
                /* You may add your own implementation here */
                alert('you closed the popup without finishing the payment');
            }
        })
    });

    function send_respon_to_form(result) {
        document.getElementById('json_callback').value = JSON.stringify(result);
        $('#submit_form').submit();
    }

</script>
@endsection
