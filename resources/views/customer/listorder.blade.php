@extends('partial.app')

@section('content')
<div class="app-content content ">
<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Daftar Order</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Order
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic Tables start -->
        <div class="row" id="basic-table">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">List Order</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                        </p>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Order</th>
                                    <th>Tanggal</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order )
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td><a style="font-weight: 500" href="/order-details/{{$order->unique_code}}">#{{$order->unique_code}}</a></td>
                                    <td>{{$order->date}}</td>
                                    <td>{{$order->gross_amount}}</td>
                                    @if ($order->status == 'settlement')
                                    <td><span class="badge rounded-pill badge-light-primary me-1">paid</span></td>
                                    @else
                                    <td><span class="badge rounded-pill badge-light-success me-1">{{$order->status}}</span></td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Basic Tables end -->

    </div>
</div>
</div>

@endsection
