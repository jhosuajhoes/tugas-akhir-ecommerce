{{-- @dd($checkouts) --}}
@extends('partial.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12">
                                <h2 class="content-header-title float-start mb-0">Checkout</h2>
                                <div class="breadcrumb-wrapper">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#">eCommerce</a>
                                        </li>
                                        <li class="breadcrumb-item active">Checkout
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                        <div class="mb-1 breadcrumb-right">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        data-feather="grid"></i></button>
                                <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item"
                                        href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span
                                            class="align-middle">Todo</span></a><a class="dropdown-item"
                                        href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span
                                            class="align-middle">Chat</span></a><a class="dropdown-item"
                                        href="app-email.html"><i class="me-1" data-feather="mail"></i><span
                                            class="align-middle">Email</span></a><a class="dropdown-item"
                                        href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span
                                            class="align-middle">Calendar</span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Checkout Place order starts -->
                {{-- <form action="" name="myform"> --}}
                @if ($count != 0)
                <div class="content">
                    <div id="place-order" class="list-view product-checkout">
                        <!-- Checkout Place Order Left starts -->
                        <div class="checkout-items">
                            @foreach ($checkouts as $checkout)
                            <div class="card ecommerce-card">
                                <div class="item-img">
                                    <a href="/product/{{$product->get(0)->id}}">
                                        <img src="{{ asset('storage/' . $checkout->product->get(0)->image)}}"
                                            alt="img-placeholder" />
                                    </a>
                                </div>
                                <div class="card-body">
                                    <div class="item-name">
                                        <h6 class="mb-0">
                                            <a href="/product/{{$product->get(0)->id}}"
                                                class="text-body">{{$checkout->product->get(0)->name}}</a>
                                        </h6>
                                    </div>
                                    <span
                                        class="text-success mb-1"><strong>{{$checkout->product->get(0)->stock}}</strong>
                                        In Stock</span>

                                    {{-- update cart --}}
                                    <form method="post" action="/checkout/{{$checkout->id}}">
                                        @csrf
                                        @method('put')
                                        <div class="item-quantity">
                                            <span class="quantity-title">Qty:</span>
                                            <div class="quantity-counter-wrapper">
                                                <div class="input-group">
                                                    <input name="quantity" type="number" id="quantity_product"
                                                        class="quantity-counter" value="{{$checkout->quantity}}" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="product-color-options mt-1">
                                            <p>{{$checkout->product->get(0)->color->name}},
                                                {{$checkout->product->get(0)->size->name}}</p>
                                            <span class="text-success"></span>
                                        </div>
                                        <button type="button" class="btn btn-sm move-cart">
                                            <span class="text-truncate">Add to Wishlist</span>
                                            <i data-feather="heart" class="align-middle me-25"></i>
                                        </button>
                                </div>
                                <div class="item-options text-center">
                                    <div class="item-wrapper">
                                        <div class="item-cost">
                                            <input type="hidden" name="price" id="harga_product"
                                                value="{{$checkout->product->get(0)->price}}">
                                            <h4 class="item-price">Rp{{$checkout->product->get(0)->price}}/pcs</h4>
                                            <p class="card-text shipping">
                                                <span class="badge rounded-pill badge-light-success">Free
                                                    Shipping</span>
                                            </p>
                                        </div>
                                    </div>
                                    <input type="hidden" name="stock" value="{{$checkout->product->get(0)->stock}}">
                                    <button type="submit" class="btn btn-outline-warning mt-2">
                                        Update
                                    </button>
                                    </form>
                                    {{-- end of update cart --}}

                                    <form action="/checkout/{{$checkout->id}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-light mt-1">
                                            <i data-feather="x" class="align-middle me-25"></i>
                                            <span>Remove</span>
                                        </button>
                                    </form>
                                </div>
                            </div>

                            @endforeach
                        </div>
                        <!-- Checkout Place Order Left ends -->
                        <!-- Checkout Place Order Right starts -->
                        <div class="checkout-options">
                            <div class="card">
                                <div class="card-body">
                                    <div class="coupons text-right">
                                        <a href="/shop" style="font-weight: 900">Continue Shopping</a>
                                    </div>
                                    <div class="price-details">
                                        <h6 class="price-title">Summary</h6>
                                        <ul class="list-unstyled">
                                            <li class="price-detail">
                                                <div class="detail-title m-t-8">Product</div>
                                                <div class="detail-amt">
                                                    Subtotal
                                                </div>
                                            </li>
                                            <hr>

                                            @foreach ($checkouts as $result)
                                            <li class="price-detail" style="font-size: 13px">
                                                <div class="detail-title m-t-8">{{$result->product->get(0)->name}}</div>
                                                <div class="detail-amt">
                                                    {{$result->price_total}}
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                        <hr />
                                        <ul class="list-unstyled">
                                            <li class="price-detail">
                                                <div class="detail-title detail-total">Total Price</div>
                                                <div class="detail-amt fw-bolder">Rp, {{$total_harga}}</div>
                                            </li>
                                        </ul>
                                        <a href="/checkout/shipment" type="button"
                                            class="btn btn-primary w-100 btn-next place-order">Place Order</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Checkout Place Order Right ends -->
                        </div>
                    </div>
                    <!-- Checkout Place order Ends -->
                </div>
                @else
                <div class="content text-center mt-5">
                    <h6>You didnt have any products in Cart</h6>
                    <a href="/shop" class="mt-3">
                        <p><strong>Let's go shopping!!!</strong></p>
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
