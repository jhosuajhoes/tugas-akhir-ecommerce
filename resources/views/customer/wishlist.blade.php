{{-- @dd($checkouts) --}}
@extends('partial.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12">
                                <h2 class="content-header-title float-start mb-0">Wishlist</h2>
                                <div class="breadcrumb-wrapper">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="/shop">eCommerce</a>
                                        </li>
                                        <li class="breadcrumb-item active">Wishlist
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Checkout Place order starts -->
                {{-- <form action="" name="myform"> --}}
                @if ($counting != 0)
                <div class="content">
                    <div id="place-order" class="list-view product-checkout">
                        <!-- Checkout Place Order Left starts -->
                        <div class="checkout-items">
                            @foreach ($wishlists as $wish)
                            <div class="card ecommerce-card">
                                <div class="item-img">
                                    <a class="img-fluid" href="/product/{{$wish->product->get(0)->id}}">
                                        <img src="{{ asset('storage/' . $wish->product->get(0)->image)}}"
                                            alt="img-placeholder" />
                                    </a>
                                </div>
                                <div class="card-body">
                                    <div class="item-name">
                                        <h6 class="mb-0"><a href="/product/{{$product->get(0)->id}}"
                                                class="text-body">{{$wish->product->get(0)->name}}</a></h6>
                                    </div>
                                    <span class="text-success mb-1"><strong>{{$wish->product->get(0)->stock}}</strong>
                                        In Stock</span>

                                    <div class="item-quantity">
                                        <span class="quantity-title">Qty : {{$wish->quantity}}</span>
                                    </div>

                                    <div class="product-color-options mt-1">
                                        <p>{{$wish->product->get(0)->color->name}},
                                            {{$wish->product->get(0)->size->name}}</p>
                                        <span class="text-success"></span>
                                    </div>
                                </div>
                            @if ($wish->product->get(0)->stock = 0)
                            <p>Product not Available, update or choose another product</p>
                            @else
                            <form action="/wishlist" method="post">
                            @csrf
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">

                                        <h4 class="item-price">Rp{{$wish->product->get(0)->price}}/pcs</h4>
                                    </div>
                                    <hr>
                                </div>
                                    <a href="/product/{{$wish->product->get(0)->id}}" class="btn btn-sm btn-success">
                                        Update
                                    </a>
                                    <input type="hidden" name="user_id" id="" value="{{auth()->user()->id}}">
                                    @if ($old_code != null)
                                    <input type="hidden" name="old_code" value="{{$old_code->unique_code}}">
                                    @else
                                    @endif
                                    <input type="hidden" name="date" value="<?php echo date("Y-m-d"); ?>">
                                    <input type="hidden" name="unique_code" value="{{$code}}">
                                    <input type="hidden" name="quantity" value="{{$wish->quantity}}">
                                    <input type="hidden" name="product_id" value="{{$wish->product->get(0)->id}}">
                                    <input type="hidden" name="price_total" value="{{$wish->quantity * $wish->product->get(0)->price}}">

                                    <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1">
                                        Checkout
                                    </button>
                                </form>
                                <form action="/wishlist/{{$wish->id}}" method="post">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-info">
                                        <i data-feather="x" class="align-middle me-25"></i>
                                        <span>Remove</span>
                                    </button>
                                </form>
                            </div>

                                @endif
                            </div>

                            @endforeach
                        </div>
                        <!-- Checkout Place Order Left ends -->
                    </div>
                </div>
                <!-- Checkout Place order Ends -->
                @else
                <div class="content text-center mt-5">
                    <h6>You didnt have any products in Wishlist</h6>
                    <a href="/shop" class="mt-3">
                        <p><strong>Let's go shopping!!!</strong></p>
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
