@extends('partial.app')

@section('content')
<div class="app-content content ">
<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper container-xxl p-0">


<div class="content-body">

{{-- Jumbotron --}}

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="container">
            <div class="row-12 d-flex justify-content-center align-items-center">
                <div class="col-md-6 mx-auto mb-3 mt-3">
                    <h4 class="detail1-judul">Menyediakan segala kebutuhan fashion anda.</h4>
                    <h1 class="judul">SKYSEA.CO</h1>
                    <h3 class="detail2-judul">Look Your Best On Your Best Day</h3>
                    <a href="/shop" class="btn btn-explore mt-2">Explore now</a>
                </div>
                <div class="col-md-6">
                    <img src="{{('img/image-landing.png')}}" class="img-fluid" alt="">
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

{{-- Our Services --}}

<div class="row">
    <div class="col-md-12">
        <div class="card h-100 mb-4 pl-5 pr-5 row-12 text-center">
            <h3 class="text-center mt-1 mb-2">Our Services</h3>
            <div class="row d-flex">
                <div class="col-sm-3">
                    <div class="icon-shapes rounded shadow">
                        <span class="iconify icon-service" data-icon="carbon:shopping-catalog" data-height="50"></span>
                    </div>
                    <div class="card-body">
                      <h4 class="card-judul">Easy Order</h4>
                      <p class="card-text">Mempermudah proses pemesanan anda, tinggal klik tombol pesanan anda siap dikirimkan.</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="icon-shapes rounded shadow">
                        <span class="iconify icon-service" data-icon="mdi:store-check-outline" data-height="50"></span>
                    </div>
                    <div class="card-body">
                      <h4 class="card-judul">Good Stuff</h4>
                      <p class="card-text">Produk dengan bahan yang berkualitas tentunya akan membuat anda semakin bersemangat menjalani hari.</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="icon-shapes rounded shadow">
                        <span class="iconify icon-service" data-icon="carbon:delivery-parcel" data-height="50"></span>
                    </div>
                    <div class="card-body">
                      <h4 class="card-judul">On Time Delivery</h4>
                      <p class="card-text">Dengan banyaknya pilihan ekspedisi yang ada tentunya menyediakan proses pengiriman kurang dari 7 hari barang sampai.</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="icon-shapes rounded shadow">
                        <span class="iconify icon-service" data-icon="dashicons:money-alt" data-height="50"></span>
                    </div>
                    <div class="card-body">
                      <h4 class="card-judul">Easy Payment</h4>
                      <p class="card-text">Proses pembayaran yang aman dan terpercaya tentunya akan memudahkan anda dalam melakukan pembayaran.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Best Seller Product --}}
<div class="row">
    <div class="col-md-10 m-auto">
        <h3 class="text-center mt-3 mb-2">Best Seller Product</h3>
        <section id="ecommerce-products" class="grid-view">
            <div class="row d-flex justify-content-center">
                @foreach ($products as $best)
                <div class="col-md-4">
                    <div class="card ecommerce-card col-sm-12">
                        <div class="item-img text-center">
                            <a href="/product/{{$best->product_id}}">
                                <img class="img-fluid card-img-top mt-1" src="{{ asset('storage/' . $best->product->get(0)->image)}}" alt="img-placeholder" alt="img-placeholder" /></a>
                        </div>
                        <div class="card-body">
                            <div class="item-wrapper">
                                <div>
                                    <h4 class="item-price">{{$best->product->get(0)->price}}</h4>
                                </div>
                            </div>
                            <h6 class="item-name">
                                <a class="text-body" href="app-ecommerce-details.html">{{$best->product->get(0)->name}}</a>
                            </h6>
                            <p class="card-text item-description">
                                {{Str::substr($best->product->get(0)->description, 0, 70) }}...
                            </p>
                        </div>
                        <div class="item-options mb-1">
                            <a href="#" class="btn btn-light btn-wishlist">
                                <i data-feather="heart"></i>
                                <span>Wishlist</span>
                            </a>
                            <a href="/product/{{$best->product_id}}" class="btn btn-primary">
                                <i data-feather="shopping-cart"></i>
                                <span class="add-to-cart">See details</span>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </section>
    </div>
</div>
<!-- E-commerce Products Ends -->
</div>
</div>
</div>
@endsection
