<?php
    if($price = '20000'){
        $category = Category::where('name',$cat_filter)->first();
            return view('customer.shop',[
                'products' => Product::where('price','<',$price)    ->groupBy('name')->get(),
                'search_results' => Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->count(),
                'categories' => Category::all(),
                'checkouts' => Cart::where('user_id',Auth::user()->id)
                                    ->where('status','=','Cart')->get()
            ]);
    }elseif($price = '20000-50000'){
        $category = Category::where('name',$cat_filter)->first();
            return view('customer.shop',[
                'products' => Product::whereBetween('price',['20000','50000'])->groupBy('name')->get(),
                'search_results' => Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->count(),
                'categories' => Category::all(),
                'checkouts' => Cart::where('user_id',Auth::user()->id)
                                    ->where('status','=','Cart')->get()
            ]);
    }
    elseif($price && $cat_filter){
        $category = Category::where('name',$cat_filter)->first();
            return view('customer.shop',[
                'products' => Product::where('price','>',$price)
                                    ->where('category_id',$category->id)->groupBy('name')->get(),
                'search_results' => Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->count(),
                'categories' => Category::all(),
                'checkouts' => Cart::where('user_id',Auth::user()->id)
                                    ->where('status','=','Cart')->get()
            ]);
    }
    //kondisi fitur searching
    elseif (request('search')) {
        return view('customer.shop',[
            'products' => Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->get(),
            'search_results' => Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->count(),
            'categories' => Category::all(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Cart')->get()
        ]);
    } else {
        return view('customer.shop',[
            'products' => Product::groupBy('name')->get(),
            'search_results' => Product::groupBy('name')->count(),
            'categories' => Category::all(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Cart')->get()
        ]);
    }


    @php $total = 0 @endphp
    @php
    $total += $checkout->product->get(0)->price * $checkout->quantity
    @endphp

?>
